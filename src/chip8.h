#ifndef CHIP8_HEADER
#define CHIP8_HEADER

#include <stdio.h>
#include <string.h>

#include "setup.h"

/* Chip8 system specifications. */

typedef struct {
  
  /* Current opcode */
  unsigned short opcode;

  /* Chip8 memory */
  unsigned char memory[4096];

  /* CPU registers */
  unsigned char V[16];

  /* Index register */
  unsigned short I;

  /* Program counter */
  unsigned short pc;

  /* Graphics (screen) */
  unsigned char gfx[64 * 32];

  /* Timers */
  unsigned char delay_timer;
  unsigned char sound_timer;

  /* Stack, and the associated stack pointer. */
  unsigned short stack[16];
  unsigned short sp;

  /* Keypad */

  unsigned char key[16];

} Chip8;


#endif
