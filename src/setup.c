#include "setup.h"

void setupGraphics(Setup * setup, unsigned int pixel_size)
{
    if (!SDL_WasInit(SDL_INIT_VIDEO))
        SDL_Init(SDL_INIT_VIDEO);
    
    setup->screen = SDL_SetVideoMode(64 * pixel_size, 32 * pixel_size, 32,
                                     SDL_HWSURFACE | SDL_DOUBLEBUF);
    
    setup->buffer = SDL_CreateRGBSurface(SDL_HWSURFACE,
                                         pixel_size, pixel_size, 32,
                                         0, 0, 0, 0);
    SDL_FillRect(setup->buffer, NULL,
                 SDL_MapRGB(setup->buffer->format, 255, 255, 255));
    
    setup->pixel_size = pixel_size;
}
