#include "chip8.h"

/*
http://www.multigesture.net/articles/how-to-write-an-emulator-chip-8-interpreter/
 */

void initialize(Chip8 * chip8)
{
  chip8->pc = 0x200;
  chip8->opcode = 0;
  chip8->I = 0;
  chip8->sp = 0;

  /* Display cleaning */
  memset(chip8->gfx, 0, sizeof(chip8->gfx));

  /* Stack cleaning */
  memset(chip8->stack, 0, sizeof(chip8->stack));

  /* Registers cleaning */
  memset(chip8->V, 0, sizeof(chip8->V));

  /* Memory cleaning */
  memset(chip8->memory, 0, sizeof(chip8->memory));

  /* Fontset loading */
  //for (i = 0; i < 80; i++)
    //chip8->memory[i] = chip8_fontset[i];

  /* Timers resetting */
  chip8->delay_timer = 0;
  chip8->sound_timer = 0;
}

void loadRom(Chip8 * chip8, char * path)
{
  // TODO
}

void emulateCycle(Chip8 * chip8)
{
  /* Fetch */
  chip8->opcode = chip8->memory[chip8->pc] << 8 | chip8->memory[chip8->pc + 1];

  /* Decode and execute. */
  
  switch (chip8->opcode & 0xF000)
    {
      

      /* ANNN: Sets I to the adress NNN */
    case 0xA000:
      chip8->I = chip8->opcode & 0x0FFF;
      chip8->pc += 2;
      break;
      
      
    }

  /* Timers updating */
  if (chip8->delay_timer > 0)
    chip8->delay_timer--;

  if (chip8->sound_timer > 0)
    if (chip8->sound_timer-- == 1)
      printf("BEEP!\n");
}

void setKeys(Chip8 * chip8)
{
  // TODO
}

void display(Chip8 * chip8, Setup * setup)
{
    int i;
    SDL_Rect rect;
    
    SDL_FillRect(setup->screen, NULL,
                 SDL_MapRGB(setup->screen->format, 0, 0, 0));
    
    for (i = 0; i < 64*32; ++i)
        if (chip8->gfx[i]) {
            rect.x = (i % 64) * setup->pixel_size;
            rect.y = (i / 64) * setup->pixel_size;
            
            SDL_BlitSurface(setup->buffer, NULL, setup->screen, &rect);
        }
    
    SDL_Flip(setup->screen);
}






