#ifndef SETUP_HEADER
#define SETUP_HEADER

#include <SDL/SDL.h>

typedef struct {
    
    /* Screen drawing surface */
    SDL_Surface * screen;
    
    /* White buffer surface */
    SDL_Surface * buffer;
    
    /* Main event handle */
    SDL_Event * event;
    
    /* Pixel size */
    unsigned int pixel_size;
    
} Setup;

void setupGraphics(Setup * setup, unsigned int pixel_size);

#endif
